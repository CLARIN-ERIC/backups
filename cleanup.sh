#!/bin/bash

STATE_FILE="/home/deploy/remote_backup_state.csv"
KEEP_TIME=7
SIMULATE_REMOVAL=1
HELP=0

#
# Process script arguments and parse all optional flags
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -r|--remove)
        SIMULATE_REMOVAL=0
        ;;
    -t|--time)
        KEEP_TIME=$2
        shift
        ;;
    -f|--file)
        STATE_FILE=$2
        shift
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
        ARG_REMAINDER_ARRAY+=("$key")
        ;;
esac
shift # past argument or value
done

if [ "${HELP}" == "1" ]; then
    echo "Usage: sh cleanup.sh <options>"
    echo ""
    echo "Defaults to running a cleanup simulation"
    echo ""
    echo "Options:"
    echo "  -r|--remove         Disable simulation mode and actually removes the files from disk"
    echo "  -t|--time           Specify the threshhold in days before files are removed. Defaults to 7 days."
    echo "  -f|--file           Specify the remote state file. Defaults to /home/deploy/remote_backup_state.csv"
    echo "  -h|--help           Show this help message"
    echo ""
    exit 0
fi

# - List files older than 7 days in the local project backup directories
# - For each file:
#   - check if it exists in the remote state file
#   - check if the local and remote checksum match
#   - delete if both all conditions are met


if [ ! -f "${STATE_FILE}" ]; then
    echo "Remote state file (${STATE_FILE}) not found"
    exit 1
fi

PROJECTS=$(sh /home/deploy/list-projects.sh)
while read -r line; do
    PROJECT_FILES=$(cd /home/deploy && find -L "${line}/" -type f -mtime +${KEEP_TIME})
    while read -r local_file; do
	if [ "${local_file}" != "" ]; then
        filename=${local_file##*/backups/}
        parent=${local_file%/backups/*}
        project_name=${parent##*/}

        #Check if the state file has an entry for the current project and filename combination
		exists_in_state_file=$(grep "${project_name}" "${STATE_FILE}" | grep "${filename}")

		if [ "${exists_in_state_file}" != "" ]; then
			LOCAL_SIZE=$(stat -c%s "${local_file}")
            LOCAL_MD5=$(md5sum "${local_file}" | cut -d ' ' -f 1)
			REMOTE_MD5=$(echo "${exists_in_state_file}" | cut -d ';' -f 3)

			if [ "${LOCAL_MD5}" == "${REMOTE_MD5}" ]; then
			    if [ "${SIMULATE_REMOVAL}" == "0" ]; then
			        rm -f "${local_file}"
			        EXIT_CODE=$?
			        if [ "${EXIT_CODE}" == "0" ]; then
			            echo "Removed ${local_file}"
			        else
			            echo "Failed to remove ${local_file}, exit code=${EXIT_CODE}"
			        fi
			    else
				    echo "Marked for removal: ${local_file}"
				fi
			else
				echo "Skipping ${local_file}. Reason: local / remote md5 mismatch (${LOCAL_MD5} != ${REMOTE_MD5})."
			fi
		else
			echo "Skipping ${local_file}. Reason: does not exist in remote state file."
		fi
	fi
    done <<< "${PROJECT_FILES}"
done <<< "${PROJECTS}"